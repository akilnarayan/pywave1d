# Computes solution to 1D periodic wave equation
#
#       u_t + v(x)*u_x = 0,          x \in [0,1)
#
# using a Fourier spectral method.

from numpy import exp, pi, sin, cos, ceil, zeros, arange
import pylab

pylab.ion()     # Turn on plot interactions (e.g. animation)

from timestepping import stepper, lserk4

########## Define domain, solution parameters
N = 80          # spatial points
T = 3.0         # Terminal time
t0 = 0.0        # Starting time
dt = 0.01       # timestep

x = arange(N, dtype=float)/N
########## 

########## Define the problem
def initial_data(x):
    return exp(sin(8*pi*x))

def v(x):
    return 1 + 0.3*sin(2*pi*x)

vx = v(x)

def rhs(t, u):
    # Return -v*u_x
    from transforms import diff
    return -vx*diff(u).real
########## 

########## Set up iteration
# Butcher tableau coefficients for RK scheme
coeffs = lserk4()

u0 = initial_data(x)
u = u0

# Number of total time instances
M = ceil((T - t0)/dt) + 1
t = zeros(M)
m = 0
t[m] = t0

line, = pylab.plot(x, u0)
pylab.show()
########## 

########## Time-stepping
while m < M-1:

    if (T-t[m]) < dt:
        dt = (T-t[m])

    u = stepper(t[m], u, rhs, dt, coeffs)
    t[m+1] = t[m] + dt

    line.set_ydata(u)
    pylab.draw()

    m += 1
