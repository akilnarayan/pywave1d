import numpy

def myfft(u):
    """Given an N-point numpy array u, computes the discrete Fourier transform,
    normalized to contained frequency-ordered coefficients in the basis
        
        exp(2*pi*i*j*t),

    where j is the frequency index."""

    from numpy.fft import fft, fftshift

    return fftshift(fft(u))/float(u.size)

def myifft(uhat):
    """The inverse of myfft: transforms frequency-ordered coefficients in the basis

        exp(2*pi*i*j*t),

    where j is the frequency index, back to point evaluations."""

    from numpy.fft import ifft, ifftshift

    return ifft(ifftshift(uhat*float(uhat.size)))

def diff(u):
    """Performs differentiation using the fast Fourier transform, assuming that
    the domain of u is [0,1], and the Fourier basis is exp(2*pi*1j*k) for
    frequencies k."""

    from math import pi
    from numpy.fft import fftfreq, fftshift

    N = u.size

    freqs = fftshift(fftfreq(N))*(2j*pi*N)

    return myifft(freqs*myfft(u))


if __name__ == "__main__":

    import numpy as np
    from math import pi

    N = 11
    x = np.arange(N,dtype=float)/N

    #u = np.exp(2*pi*1j*3*x)
    u = np.cos(2*pi*x)

    uhat = myfft(u)

    u2 = myifft(uhat)

    udiff = diff(u)
