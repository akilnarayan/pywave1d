Solves the 1D linear periodic wave equation (non-costant wavespeed) using a Fourier spectral method.
