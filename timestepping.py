import numpy

def lserk4():
    """Returns coefficients for a low-storage explicit SSP RK-4 time-stepper."""

    coeffs = dict()

    coeffs['a'] = numpy.array( \
                              [ 0., \
                                -567301805773./1357537059087, \
                                -2404267990393./2016746695238, \
                                -3550918686646./2091501179385, \
                                -1275806237668./842570457699], \
                               dtype=float)

    coeffs['b'] = numpy.array( \
                              [ 1432997174477./9575080441755, \
                                5161836677717./13612068292357, \
                                1720146321549./2090206949498, \
                                3134564353537./4481467310338, \
                                2277821191437./14882151754819], \
                              dtype=float)

    coeffs['c'] = numpy.array( \
                              [ 0.0, \
                                1432997174477.0/9575080441755.0, \
                                2526269341429.0/6820363962896.0, \
                                2006345519317.0/3224310063776.0, \
                                2802321613138.0/2924317926251.0, \
                                1.0 \
                              ], \
                              dtype=float);

    coeffs['p'] = 5;

    return coeffs

def fe():
    """Returns coefficients for Forward Euler time-stepping."""

    assert False


def stepper(t, y, f, dt, coeffs):
    """Takes one step performing a single dt-sized timestep given explicit
    Runge-Kutta coefficients. The input f is a function with the calling syntax:

       f(t, y),

    returning the right-hand side of an ODE

        y' = f(t,y).

    y is the current state (numpy array), and t (scalar) the current time.
    """

    local_times = t + dt*coeffs['c'];

    k = y.copy()

    for q in range(coeffs['p']):

        k = coeffs['a'][q]*k + dt*f(local_times[q], y)
        y += coeffs['b'][q]*k

    return y

if __name__ == "__main__":

    from math import pi
    import numpy as np

    # Solve y' = a*y
    a = -1/pi

    t0 = 0.
    T = 3.

    y0 = 1

    t = 0.
    dt = 0.001

    coeffs = lserk4()

    N = np.ceil((T - t0)/dt)
    y = np.zeros(N)
    y[0] = y0;
    n = 0

    def f(tt,yy):
        return a*yy

    while n < N-1:

        if (T-t) < dt:
            dt = (T-t)

        y[n+1] = stepper(t, y[n], f, dt, coeffs)
        n += 1
